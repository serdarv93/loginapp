//
//  RegisterViewController.swift
//  loginApp
//
//  Created by Vladimir Serdar on 18/11/2019.
//  Copyright © 2019 Vladimir Serdar. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var emailFieldValidation: UILabel!
    @IBOutlet weak var firstNameFieldValidation: UILabel!
    @IBOutlet weak var lastNameFieldValidation: UILabel!
    @IBOutlet weak var passwordFieldValidation: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailFieldValidation.isHidden = true
        firstNameFieldValidation.isHidden = true
        lastNameFieldValidation.isHidden = true
        passwordFieldValidation.isHidden = true
    }
    
    @IBAction func emailFieldChanged(_ sender: Any) {
        emailFieldValidation.isHidden = true
    }
    
    @IBAction func firstNameFieldChanged(_ sender: Any) {
        firstNameFieldValidation.isHidden = true
    }

    @IBAction func lastNameFieldChanged(_ sender: Any) {
        lastNameFieldValidation.isHidden = true
    }
    
    @IBAction func passwordFieldChanged(_ sender: Any) {
        passwordFieldValidation.isHidden = true
    }
    
    @IBAction func onRegisterClick(_ sender: Any) {
        
        guard let email = emailField.text, !(emailField.text?.isEmpty ?? false) else {
            emailFieldValidation.text = "Please enter email!"
            emailFieldValidation.isHidden = false
            return
        }
        
        if !isValidEmail(email: email) {
            emailFieldValidation.text = "Please enter correct email adress!"
            emailFieldValidation.isHidden = false
        }
        
        guard let _ = firstNameField.text, !(firstNameField.text?.isEmpty ?? false) else {
            firstNameFieldValidation.text = "Please enter email!"
            firstNameFieldValidation.isHidden = false
            return
        }
        
        guard let _ = lastNameField.text, !(lastNameField.text?.isEmpty ?? false) else {
            lastNameFieldValidation.text = "Please enter email!"
            lastNameFieldValidation.isHidden = false
            return
        }
        
        guard let _ = passwordField.text, !(passwordField.text?.isEmpty ?? false) else {
            passwordFieldValidation.text = "Please enter email!"
            passwordFieldValidation.isHidden = false
            return
        }

        self.serverRegister(email: self.emailField.text ?? "",firstName: self.firstNameField.text ?? "",lastName: self.lastNameField.text ?? "",password: self.passwordField.text ?? "")
    }
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    func serverRegister(email:String,firstName:String,lastName:String,password:String){
        
        let json : [String : Any] = [
        "first_name":firstName,
        "last_name":lastName,
        "email":email,
        "password":password
        ]
        
        ApiService.registerUser(json: json, completionData: { (data) in
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "viewController")
                self.present(vc, animated: true, completion: nil)
                
            }
            
        }, completionError: { (error) in
            print(error)
        })
        
        
        
    }}
