//
//  ApiService.swift
//  loginApp
//
//  Created by Vladimir Serdar on 20/11/2019.
//  Copyright © 2019 Vladimir Serdar. All rights reserved.
//

import UIKit
import Alamofire

class ApiService {
    
    static func registerUser(json : [String:Any], completionData : @escaping (_ result : Data) -> Void,completionError : @escaping (Error) -> Void){
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)

        let url = URL(string: "https://ios-api-v2.getsandbox.com/create_account")!
        
        let session = URLSession.shared

        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"

        request.httpBody = jsonData
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        print(json)

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            if error != nil {
                completionError(error!)
            }
            
            if data != nil {
                completionData(data!)
            }
            
        })

        task.resume()
        
    }
    
    static func loginUser(email : String,password : String, completionData : @escaping (_ result : [String:Any]) ->Void,completionError : @escaping (Error) -> Void) {
        
        let url = URL(string: Const.BaseUrl + "/login?email=\(email)&password=\(password)")!
        
        AF.request(url).validate().responseJSON { (response) in
            if let error = response.error {
                completionError(error)
            }
            
            if let data = response.value as? [String:Any] {
                print(data)
                completionData(data)
            }
            
        }
        
        /*let session = URLSession.shared
        
        let request = URLRequest(url: url)

        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            if let error = error {
                completionError(error)
            }

            if let data = data {
                do{
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        completionData(json)
                        
                    }
                    
                } catch let error {
                    print(error.localizedDescription)
                  }
                
            }
           
        })

        task.resume() */
    }
    
}
