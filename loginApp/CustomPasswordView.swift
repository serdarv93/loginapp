//
//  CustomPasswordView.swift
//  loginApp
//
//  Created by Vladimir Serdar on 20/11/2019.
//  Copyright © 2019 Vladimir Serdar. All rights reserved.
//

import UIKit


enum LINE_POSITION {
    case LINE_POSITION_TOP
    case LINE_POSITION_BOTTOM
}

extension UIView {
    func addLine(position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)

        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))

        switch position {
        case .LINE_POSITION_TOP:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        }
    }
}

class CustomPasswordView: UIView {
    
    var textField : UITextField?
    var errorLabel : UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViewConstraints()
        
        let image = setupImageView()
        
        addSubview(image)
        
        setupImageViewConstraints(image: image)
        
        let textField : UITextField = setupTextField()
        
        self.textField = textField
        
        addSubview(textField)
        
        setupTextFieldConstraints(textField: textField, image: image)
        
        let errorLabel : UILabel = setupErrorLabel()
        
        self.errorLabel = errorLabel
        
        addSubview(errorLabel)
        
        setupErrorLabelConstratints(errorLabel: errorLabel, image: image, textField: textField)
        
    }
    
    private func setupViewConstraints(){
        backgroundColor = UIColor.white
        layoutMargins.left = CGFloat(20)
        layoutMargins.right = CGFloat(20)
        layoutMargins.top = CGFloat(20)
        layoutMargins.bottom = CGFloat(20)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupImageView() -> UIImageView {
        let imageName = "lockIcon.png"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        
        imageView.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        
        return imageView
    }
    
    private func setupImageViewConstraints(image : UIImageView){
        image.translatesAutoresizingMaskIntoConstraints = false
        image.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
        image.heightAnchor.constraint(equalToConstant: 20).isActive = true
        image.widthAnchor.constraint(equalToConstant: 20).isActive = true
        image.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    private func setupTextField() -> UITextField {
        
        let textField = UITextField()
        
        textField.placeholder = "Password"
        textField.isSecureTextEntry = true
        
        return textField
    }
    
    private func setupTextFieldConstraints(textField: UITextField,image: UIImageView){
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 20).isActive = true
        textField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20).isActive = true
        textField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        textField.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    private func setupErrorLabel() -> UILabel {
        
        let label : UILabel = UILabel()
        label.text = "Somethings wrong!!"
        label.textColor = .red
        label.isHidden = true
        
        return label
    }
    
    private func setupErrorLabelConstratints(errorLabel : UILabel,image: UIImageView,textField: UITextField) {
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        errorLabel.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 20).isActive = true
        errorLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20).isActive = true
        errorLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        errorLabel.topAnchor.constraint(equalTo: textField.bottomAnchor).isActive = true
    }
    
    public func showError(error: String?){
        
        self.textField!.addLine(position: LINE_POSITION.LINE_POSITION_BOTTOM, color: .red, width: 1)
        
        self.errorLabel!.text = error
        
        self.errorLabel?.isHidden = false
        
    }
    
    public func hideError(){
        
        self.textField!.addLine(position: LINE_POSITION.LINE_POSITION_BOTTOM, color: .white, width: 1)
        
        self.errorLabel?.isHidden = true
    }
}
