//
//  ViewController.swift
//  loginApp
//
//  Created by Vladimir Serdar on 18/11/2019.
//  Copyright © 2019 Vladimir Serdar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var emailField:UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBAction func secScreenButton(){
        print("button pressed")
    }
    @IBOutlet weak var emailLabelValidation: UILabel!
    @IBOutlet weak var passwordLabelValidation: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailLabelValidation.isHidden = true
        passwordLabelValidation.isHidden = true
    }
    
    @IBAction func emailFieldChanged(_ sender: Any) {
        emailLabelValidation.isHidden = true
        passwordLabelValidation.isHidden = true
        
    }
    
    @IBAction func passwordFieldChanged(_ sender: Any) {
        passwordLabelValidation.isHidden = true
    }
    @IBAction func login(){
        
        guard let email = emailField.text, !(emailField.text?.isEmpty ?? false) else {
            emailLabelValidation.text = "Please enter email!"
            emailLabelValidation.isHidden = false
            return
        }
        
        if !isValidEmail(email: email) {
            emailLabelValidation.text = "Please enter correct email adress!"
            emailLabelValidation.isHidden = false
        }
        
        guard let _ = passwordField.text, !(passwordField.text?.isEmpty ?? false) else {
            passwordLabelValidation.text = "Please enter password!"
            passwordLabelValidation.isHidden = false;
            return
        }
        
        if(self.emailField.text != "" && self.passwordField.text != ""){
            self.serverLogin(email :self.emailField.text,password :self.passwordField.text)
            
        }
    }
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }


    @IBAction func register(){
        
    }
    
    func serverLogin(email:String?,password:String?){
        
        ApiService.loginUser(email: email!, password: password!, completionData: { (data) in
             
             let status : String? = data["status"] as! String?
             
             DispatchQueue.main.async {
                 if(status != nil && status == "ok"){
                     let defaults = UserDefaults.standard
                     defaults.set(self.emailField.text ?? "" ,forKey: "email")
                     defaults.set(self.passwordField.text ?? "", forKey:"password")
                     let storyboard = UIStoryboard(name: "Main", bundle: nil)
                     let vc = storyboard.instantiateViewController(withIdentifier: "homeViewController")
                     
                     self.navigationController?.show(vc, sender: self)
                 } else {
                     self.passwordLabelValidation.text = "Check your credentials and try again!"
                     self.passwordLabelValidation.isHidden = false
                 }
             }
         }, completionError : { (error) in
            print(error)
        })
        
    }
}

