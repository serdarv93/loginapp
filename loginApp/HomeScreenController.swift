//
//  LaunchScreenController.swift
//  loginApp
//
//  Created by Vladimir Serdar on 19/11/2019.
//  Copyright © 2019 Vladimir Serdar. All rights reserved.
//

import UIKit

class HomeScreenController: UIViewController {
    
    var passView : CustomPasswordView?
    var switchError : Bool = true

    @IBOutlet weak var SplashScreenImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationController?.navigationBar.isTranslucent = false
        
        let logoutButton = UIBarButtonItem(title: "Logout",style: .plain, target: self, action: #selector(logout))
        
        navigationItem.rightBarButtonItem = logoutButton
        
        let defaults = UserDefaults.standard
        print(defaults.string(forKey: "email"))
        
        let passView : CustomPasswordView = {
            
            let passView = CustomPasswordView()
            
            self.passView = passView
            
            passView.frame.size = CGSize(width: self.view.frame.size.width, height: 200.0)

            return passView
        } ()
        
        view.addSubview(passView)

    }
    
    @objc func logout(){
        
        /*self.navigationController?.popViewController(animated: true)
        
        let defaults = UserDefaults.standard
        print(defaults.string(forKey: "email"))
        UserDefaults.standard.removePersistentDomain(forName:Bundle.main.bundleIdentifier!)

        UserDefaults.standard.synchronize()
        
        print("Logout")*/
        
        if(switchError){
            self.passView?.showError(error: "Ne valja")
            switchError = !switchError
        } else {
            self.passView?.hideError()
            switchError = !switchError
        }
        
    }

}
